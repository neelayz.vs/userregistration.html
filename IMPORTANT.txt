using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
namespace WebApplication1.Controllers
{
    [Route("api/[controller]/[action]/{id?}")]
    [ApiController]
    public class RegistrationFormController : ControllerBase
    {
        private IConfiguration _configuration;
        public RegistrationFormController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpPost]
        public string InsertingUserDetails(RegisterDetails user)
        {
            string connectionString = _configuration.GetConnectionString("DBConnectionString");
            SqlConnection con = new SqlConnection(connectionString);
            try
            {
                con.Open();
                string query = "INSERT INTO RegisteredDetails (FirstName, LastName, MobileNo, EmailAddress, Password) " +
                    "VALUES(@FirstName, @LastName, @MobileNo, @EmailAddress, @Password)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@FirstName", user.FirstName);
                cmd.Parameters.AddWithValue("@LastName", user.LastName);
                cmd.Parameters.AddWithValue("@MobileNo", user.MobileNo);
                cmd.Parameters.AddWithValue("@EmailAddress", user.EmailAddress);
                cmd.Parameters.AddWithValue("@Password", user.Password);
                cmd.ExecuteNonQuery();
                con.Close();
                return "User Registered Successfully";
            }
            catch (Exception e)
            {
                return ("Error: " + e.Message);
            }

        }
        [HttpPost]
        public string LoginDetails(RegisterDetails user)
        {
            string connectionString = _configuration.GetConnectionString("DBConnectionString");
            SqlConnection con = new SqlConnection(connectionString);
            try
            {
                con.Open();
                string query1 = "Select * from RegisteredDetails where EmailAddress = @EmailAddress";
                string query2 = "Select * from RegisteredDetails where Password = @Password";
                SqlCommand cmd1 = new SqlCommand(query1, con);
                SqlCommand cmd2 = new SqlCommand(query2, con);
                cmd1.Parameters.AddWithValue("@EmailAddress", user.EmailAddress);
                cmd2.Parameters.AddWithValue("@Password", user.Password);
                SqlDataReader dr1 = cmd1.ExecuteReader();
                SqlDataReader dr2 = cmd2.ExecuteReader();
                if (dr1.HasRows == true && dr2.HasRows == true)
                {
                    return ("Login Successfully");
                }
                else
                {
                    return ("Invalid Email ID and Password");
                }

            }

            catch (Exception e)
            {
                return (e.Message);
            }

        }
        [HttpPost]
        public string AnagramCheck(RegisterDetails words)
        {
            try
            {
                char[] ch1 = words.Word1.ToLower().ToCharArray();
                char[] ch2 = words.Word2.ToLower().ToCharArray();
                Array.Sort(ch1);
                Array.Sort(ch2);
                string sort1 = new string(ch1);
                string sort2 = new string(ch2);
                if (sort1 == sort2)
                {
                    return ("It is Anagram");

                }
                else
                {
                    return ("It is not a  Anagram");
                }

            }

            catch (Exception e)
            {
                return (e.Message);
            }

        }
        [HttpPost]
        public int Stringspilt(RegisterDetails words)
        {
            try
            {
                string[] ssize = words.Word1.Split(new char[0]);
                var f1 = ssize[0];
                var f2 = ssize[1];
                var f3 = ssize[2];

                var f4 = f1.Split("=");
                var f5 = f2.Split("=");

                var f6 = f3.Split("=");
                var f7 = f6[1].Split("A");
                var f8 = f7[1].Split("B");

                if (f8[0] == "+")
                {
                    var a = Convert.ToInt32(f4[1]);
                    var b = Convert.ToInt32(f5[1]);
                    int c = a + b;
                    return c;
                }
                else if (f8[0] == "-")
                {
                    var a = Convert.ToInt32(f4[1]);
                    var b = Convert.ToInt32(f5[1]);
                    int c = a - b;
                    return c;
                }
                else if (f8[0] == "*")
                {
                    var a = Convert.ToInt32(f4[1]);
                    var b = Convert.ToInt32(f5[1]);
                    int c = a * b;
                    return c;
                }
                else if (f8[0] == "/")
                {
                    var a = Convert.ToInt32(f4[1]);
                    var b = Convert.ToInt32(f5[1]);
                    int c = a / b;
                    return c;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {
                return 001;
            }

        }
        [HttpPost]
        public string RepeatingCharacters(RegisterDetails words)
        {
            string inputString = words.Word1.ToLower();
            string resultString = string.Empty;
            var unique = new HashSet<char>(inputString);
            foreach (char c in unique)
            {
                resultString += c;                
            }
            return (resultString);
        }
        [HttpPost]
        public string HallSize(RegisterDetails Tiles)
        {
            double AnsWidth;
            double AnsHeight;
            AnsWidth = Tiles.Width * 1.33;
            AnsHeight = Tiles.Length * 1.33;
            var ans1 = AnsWidth.ToString();
            var ans2 = AnsHeight.ToString();
            return ("The hall is "+  ans1  +" feet wide and " +  ans2 + " feet long.");
        }
    }
    public class RegisterDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Word1 { get; set; }
        public string Word2 { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
    }
}