using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterationForm : ControllerBase
    {
        private IConfiguration _configuration;
        public RegisterationForm(IConfiguration configuration)
        {
            _configuration = configuration;
        }   
        [HttpPost]
        public string InsertingUserDetails(RegisteredDetails user)
        {
            string connectionString = _configuration.GetConnectionString("DBConnectionString");
            SqlConnection con = new SqlConnection(connectionString);
            try
            {
                con.Open();
                string query = "INSERT INTO RegisteredDetails (FirstName, LastName, MobileNo, EmailAddress, Password) " +
                    "VALUES(@FirstName, @LastName, @MobileNo, @EmailAddress, @Password)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@FirstName", user.FirstName);
                cmd.Parameters.AddWithValue("@LastName", user.FirstName);
                cmd.Parameters.AddWithValue("@@MobileNo", user.FirstName);
                cmd.Parameters.AddWithValue("@EmailAddress", user.FirstName);
                cmd.Parameters.AddWithValue("@Password", user.FirstName);
                SqlDataReader dr = cmd.ExecuteReader();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            con.Close();
            return "User Registered Successfully";
        }

    }
    public class RegisteredDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int MobileNo { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }

    }
}